<?php
/**
 * sou_3 custom post types and taxonomies.
 *
 *  
 *
 * @package sou_3
 */
/**
* Register Taxonomies
*/
// Register Custom Taxonomy
function sou_3_create_designs() {

	$labels = array(
		'name'                       => _x( 'Designs', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Design', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Designs', 'text_domain' ),
		'all_items'                  => __( 'Designs', 'text_domain' ),
		'parent_item'                => __( 'Parent Design', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Design:', 'text_domain' ),
		'new_item_name'              => __( 'New Design Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Design', 'text_domain' ),
		'edit_item'                  => __( 'Edit Design', 'text_domain' ),
		'update_item'                => __( 'Update Design', 'text_domain' ),
		'view_item'                  => __( 'View Design', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate Designs with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or Remove Designs', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Designs', 'text_domain' ),
		'search_items'               => __( 'Search Designs', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                       => 'furniture/design',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'designs', array( 'featured-hero', 'pieces', 'teasers' ), $args );

}
add_action( 'init', 'sou_3_create_designs', 0 );

// Register Custom Taxonomy
function sou_3_create_collections() {

	$labels = array(
		'name'                       => _x( 'Collections', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Collection', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Collections', 'text_domain' ),
		'all_items'                  => __( 'All Collections', 'text_domain' ),
		'parent_item'                => __( 'Parent Collection', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Collection:', 'text_domain' ),
		'new_item_name'              => __( 'New Collection Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Collection', 'text_domain' ),
		'edit_item'                  => __( 'Edit Collection', 'text_domain' ),
		'update_item'                => __( 'Update Collection', 'text_domain' ),
		'view_item'                  => __( 'View Collection', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'collection', array( 'featured-hero', 'teasers', 'pieces' ), $args );

}
add_action( 'init', 'sou_3_create_collections', 0 );

/**
* Custom Post Types
*/

// Featured Heros

function sou_3_create_featured_heroes() {

	$labels = array(
		'name'                  => _x( 'Featured Heroes', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Featured Hero', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Featured Sliders', 'text_domain' ),
		'name_admin_bar'        => __( 'Hero', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Hero:', 'text_domain' ),
		'all_items'             => __( 'All Heroes', 'text_domain' ),
		'add_new_item'          => __( 'Add New Hero', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Hero', 'text_domain' ),
		'edit_item'             => __( 'Edit Hero', 'text_domain' ),
		'update_item'           => __( 'Update Hero', 'text_domain' ),
		'view_item'             => __( 'View Hero', 'text_domain' ),
		'search_items'          => __( 'Search Heroes', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Slide', 'text_domain' ),
		'set_featured_image'    => __( 'Set Featured Slide', 'text_domain' ),
		'remove_featured_image' => __( 'Remove Featured Slide', 'text_domain' ),
		'use_featured_image'    => __( 'Use as Featured Slide', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Featured Hero', 'text_domain' ),
		'description'           => __( 'Featured hero slider', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'post_tag', 'designs' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'rewrite'               => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'featured-hero', $args );

}
add_action( 'init', 'sou_3_create_featured_heroes', 0 );

// Teasers

// Register Custom Post Type
function sou_3_create_teasers() {

	$labels = array(
		'name'                  => _x( 'Post Types', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Teaser', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Teasers', 'text_domain' ),
		'name_admin_bar'        => __( 'Teaser', 'text_domain' ),
		'archives'              => __( 'Teaser Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Teaser', 'text_domain' ),
		'all_items'             => __( 'All Teasers', 'text_domain' ),
		'add_new_item'          => __( 'Add New Teaser', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Teaser', 'text_domain' ),
		'edit_item'             => __( 'Edit Teaser', 'text_domain' ),
		'update_item'           => __( 'Update Teaser', 'text_domain' ),
		'view_item'             => __( 'View Teaser', 'text_domain' ),
		'search_items'          => __( 'Search Teaser', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Teaser', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to Teaser', 'text_domain' ),
		'items_list'            => __( 'Teasers List', 'text_domain' ),
		'items_list_navigation' => __( 'Teasers List Navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Teasers List', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Teaser', 'text_domain' ),
		'description'           => __( 'Graphical teasers with links', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'designs', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'rewrite'               => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'teasers', $args );

}
add_action( 'init', 'sou_3_create_teasers', 0 );

// Register Custom Post Type
function sou_3_create_pieces() {

	$labels = array(
		'name'                  => _x( 'Pieces', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Piece', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Pieces', 'text_domain' ),
		'name_admin_bar'        => __( 'Pieces', 'text_domain' ),
		'archives'              => __( 'Pieces', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent', 'text_domain' ),
		'all_items'             => __( 'All PIeces', 'text_domain' ),
		'add_new_item'          => __( 'Add New Piece', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Piece', 'text_domain' ),
		'edit_item'             => __( 'Edit Piece', 'text_domain' ),
		'update_item'           => __( 'Update Piece', 'text_domain' ),
		'view_item'             => __( 'View Piece', 'text_domain' ),
		'search_items'          => __( 'Search Piece', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Pieces List', 'text_domain' ),
		'items_list_navigation' => __( 'Items List Navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Items List', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'furniture',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Piece', 'text_domain' ),
		'description'           => __( 'South of Urban Furniture', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'post_tag', 'designs' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'furniture',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'pieces', $args );

}
add_action( 'init', 'sou_3_create_pieces', 0 );
 




