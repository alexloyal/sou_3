<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sou_3
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
<meta name="description" content="<?php bloginfo( 'description' ); ?>">

</head>
<!-- header.php -->


<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sou_3' ); ?></a>

	<div id="header-container" class="row">
		<header id="masthead" class="site-header col-lg-10 col-lg-offset-1" role="banner">
			<div class="site-branding col-lg-9 col-md-8 col-sm-6 col-xs-12">
				

				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

 
			</div><!-- .site-branding -->

			<div id="nav-menu-container" class=" col-lg-3 col-md-4 col-sm-6 col-xs-12"> <!-- menu container -->

				<div class="row">
					<div class="col-xs-12">
						<nav id="site-navigation" class="main-navigation" role="navigation">
							<button class="menu-toggle" aria-controls="navigation-menu" aria-expanded="false"><?php esc_html_e( 'Navigation Menu', 'sou_3' ); ?></button>
							<?php wp_nav_menu( array( 'theme_location' => 'navigation-menu', 'menu_id' => 'navigation-menu' ) ); ?>
						</nav><!-- #site-navigation -->				
					</div>
					<div class="col-xs-12">
						<?php wp_nav_menu( array( 'theme_location' => 'social-media-menu', 'menu_id' => 'social-media-menu' ) ); ?>
					</div>
				</div>
			</div><!-- #nav-menu-container -->
		</header><!-- #masthead -->
	</div>

	<div id="content" class="site-content row">
