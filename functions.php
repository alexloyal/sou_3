<?php
/**
 * sou_3 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sou_3
 */

if ( ! function_exists( 'sou_3_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sou_3_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on sou_3, use a find and replace
	 * to change 'sou_3' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'sou_3', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'navigation-menu' => esc_html__( 'Navigation Menu', 'sou_3' ),
		'social-media-menu' => esc_html__( 'Social Media Menu', 'sou_3' ),

	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'sou_3_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'sou_3_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sou_3_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sou_3_content_width', 640 );
}
add_action( 'after_setup_theme', 'sou_3_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sou_3_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sou_3' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sou_3_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sou_3_scripts() {
	wp_enqueue_style( 'sou_3-style', get_stylesheet_uri() );

	/*wp_enqueue_style( 'bootstrap-theme', get_stylesheet_directory_uri() . '/css/bootstrap-theme.css' );*/
	wp_enqueue_style( 'bootstrap-theme.css.map', get_stylesheet_directory_uri() . '/css/bootstrap-theme.css.map' );
	wp_enqueue_style( 'bootstrap-theme.min.css', get_stylesheet_directory_uri() . '/css/bootstrap-theme.min.css' );
	/* wp_enqueue_style( 'bootstrap.css', get_stylesheet_directory_uri() . '/css/bootstrap.css' ); */
	wp_enqueue_style( 'bootstrap.css.map', get_stylesheet_directory_uri() . '/css/bootstrap.css.map' );
	wp_enqueue_style( 'bootstrap.min.css', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/main.css' );

	wp_enqueue_script( 'jQuery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js', array(), '2.2.0', false );
	wp_enqueue_script( 'sou_3-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'sou_3-main', get_template_directory_uri() . '/js/main.js', array(), '1.0', true );
	wp_enqueue_script( 'sou_3-plugins', get_template_directory_uri() . '/js/plugins.js', array(), '1.0', true );
	wp_enqueue_script( 'bootstrap.js', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array(), '3.3.1', true );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', array(), '2.8.3', true );
	wp_enqueue_script( 'sou_3-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/vendor/waypoints/lib/noframework.waypoints.min.js' , array(), '4.0', false );

	wp_localize_script( 'sou_3-main', 'sou_3_ajax', array(

			'ajaxurl'	=>	admin_url( 'admin-ajax.php', true )

		));

	// print_r($wp_query);

	


	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}



	
}
add_action( 'wp_enqueue_scripts', 'sou_3_scripts' );

add_action( 'wp_ajax_sou_3_load_posts', 'sou_3_load_posts');
add_action( 'wp_ajax_nopriv_sou_3_load_posts', 'sou_3_load_posts'); 

 

// Remove auto generated feed links

function sou_3_remove_tags() {
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action('wp_head', 'wp_generator');
}

add_action( 'after_setup_theme', 'sou_3_remove_tags' );


// Adding Custom URL meta boxes to Teasers.

add_action( 'load-post.php', 'sou_3_custom_url_meta_box_setup');
add_action( 'load-post-new.php', 'sou_3_custom_url_meta_box_setup');

function sou_3_custom_url_meta_box_setup(){

	add_action( 'add_meta_boxes', 'sou_3_add_custom_url_meta_box' );

	add_action( 'save_post', 'sou_3_save_teaser_custom_url', 10, 2 );

	
}

function sou_3_add_custom_url_meta_box(){

	add_meta_box( 
			'sou_3_teaser_url', 
			esc_html__('Destination URL',' domain'), 
			'sou_3_display_custom_teaser_url', 
			'teasers', 
			'normal', 
			'high', 
			'' );
}

function sou_3_display_custom_teaser_url($object, $box){ ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'sou_3_teaser_custom_url_field_nonce' ); ?>

	<p>
		<label for="sou-3-custom-teaser-url">	<?php _e( "Add a destination URL:", 'domain' ); ?>	</label>
		
		<input class="widefat" type="text" name="sou-3-custom-teaser-url" id="sou-3-custom-teaser-url" 
			value="<?php echo esc_attr(get_post_meta($object->ID,'sou-3-custom-teaser-url', true) ); ?>" size="30" />
	</p>
	
	<?php
}

function sou_3_save_teaser_custom_url($post_id,$post){
	if (!isset($_POST['sou_3_teaser_custom_url_field_nonce']) || !wp_verify_nonce( $_POST['sou_3_teaser_custom_url_field_nonce'], basename( __FILE__ ) ) ) 
		
		return $post_id;
	
	$post_type = get_post_type_object('teasers' );

	if (!current_user_can($post_type->cap->edit_post,$post_id ) ) 
		
		return $post_id;


	$sou_3_custom_teaser_url_value = (isset($_POST['sou-3-custom-teaser-url']) ? $_POST['sou-3-custom-teaser-url'] : 'false');


	
	$meta_key = 'sou-3-custom-teaser-url';
	
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	if ($sou_3_custom_teaser_url_value && '' == $meta_value)
		
		add_post_meta( $post_id, $meta_key, $sou_3_custom_teaser_url_value, true );

	elseif ($sou_3_custom_teaser_url_value && $sou_3_custom_teaser_url_value != $meta_value)

		update_post_meta( $post_id, $meta_key, $sou_3_custom_teaser_url_value );

	elseif ('' == $sou_3_custom_teaser_url_value && $meta_value) 
		
		delete_post_meta( $post_id, $meta_key, $meta_value );	
	
}

function strip_shortcode_gallery( $content ) {
    preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );

    if ( ! empty( $matches ) ) {
        foreach ( $matches as $shortcode ) {
            if ( 'gallery' === $shortcode[2] ) {
                $pos = strpos( $content, $shortcode[0] );
                if( false !== $pos ) {
                    return substr_replace( $content, '', $pos, strlen( $shortcode[0] ) );
                }
            }
        }
    }

    return $content;
}

/**
 * Implement custom post types and taxonomies.
 */
require get_template_directory() . '/inc/custom-post-types-taxonomies.php';


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
