<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sou_3
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer row footer" role="contentinfo">
		<div class="site-info col-lg-3 col-xs-12">
			
			
			
			<h5 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h5>


		</div><!-- .site-info -->

		<div class="col-lg-6 col-xs-6">
			<?php wp_nav_menu( array( 'theme_location' => 'navigation-menu', 'menu_id' => 'navigation-menu' ) ); ?>
		</div>

		<div class="col-lg-3 col-xs-6"></div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
