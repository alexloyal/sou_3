<?php
/**
 * The template for displaying Design archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sou_3
 */

get_header(); ?>
<!-- taxonomy-design!  -->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// WP_Query arguments
		$args = array (
			'post_type'              => array( 'pieces' ),
			'post_status'            => array( 'publish' ),
			'nopaging'               => true,
			'paged'                  => 'paged',
			'posts_per_page'         => '6',
			'orderby'                => 'title',
		);

		// The Query
		$piecesBy_Design = new WP_Query( $args );

 
		if ( $piecesBy_Design->have_posts() ) : ?>

			<header class="page-header row">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description col-xs-12">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( $piecesBy_Design->have_posts() ) : $piecesBy_Design->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; 

		// Restore original Post Data
		wp_reset_postdata();

		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
