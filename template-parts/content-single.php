<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sou_3
 */

?>
<!-- content -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12'); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php sou_3_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		

		<?php 

	         $content = strip_shortcode_gallery( get_the_content() );                                        
			 $content = str_replace( ']]>', ']]&gt;', apply_filters( 'the_content', $content ) ); 

		 ?>
 						

 				  <div id="gallery-container" class="col-xs-12 col-sm-6">
	 				  <div id="<?php the_title(); ?>-gallery" class="carousel slide" data-ride="carousel">
	 				            <div class="carousel-inner" role="listbox">

	 				            <?php if ( get_post_gallery() ) :
			                        $gallery = get_post_gallery( get_the_ID(), false );

			                        $counter = 0;?>

			                        <ol class="carousel-indicators">
	            	                        <?php
	            		                        /* Loop through all the image and output carousel indicators */
	            		                        foreach( $gallery['src'] as $src ) : 
	            		                        if ($counter < 1) { ?>
	            		                        	<li data-target="#<?php the_title(); ?>-gallery" data-slide-to="<?php echo $counter; ?>" class="active"></li>

	            		                        	<?php   } else { ?>
	            		                        	<li data-target="#<?php the_title(); ?>-gallery" data-slide-to="<?php echo $counter; ?>"></li>		                        		                        	<?php
	            		                        }

	            	                        	$counter++;
	            	                        endforeach; ?>
			                        </ol>	
			                        <?php
			                        	$counter = 0;
				                        /* Loop through all the image and output them one by one */
				                        foreach( $gallery['src'] as $src ) :   
				                        if ($counter < 1) {
				                        	?>
				                        	<div class="item active">
				                        		<img src="<?php echo $src; ?>" class="col-xs-12" alt="Gallery image" />
				                        	</div>

				                        	<?php
				                        } else {
				                        	?>
				                        	<div class="item">
				                        		<img src="<?php echo $src; ?>" class="col-xs-12" alt="Gallery image" />
				                        	</div>
				                        	<?php
				                        }

			                        	$counter++;
			                        endforeach; ?>
			            	<?php endif; ?>
			            		</div>
	 				</div> <!-- <?php the_title(); ?>-gallery -->
 				  </div>

        <div id="content" class="col-xs-12 col-sm-3">
            <?php echo $content; ?>
        </div> <!-- id="content" -->
		<?php
			 

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sou_3' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php sou_3_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
