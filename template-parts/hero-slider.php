<?php
/**
 * Template part for displaying hero slider.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sou_3
 */

?>
<!-- hero-slider -->
<div class="row">

  <div id="hero-carousel" class="carousel slide col-xs-12" data-ride="carousel">



    <?php 
            
            // WP_Query arguments
            $args = array (
              'post_type'              => array( 'featured-hero' ),
              'post_status'            => array( 'publish' ),
              'tax_query'              => array(
                                                array(
                                                      'taxonomy' => 'designs',
                                                      'field'    => 'slug',
                                                      'terms'    => 'home',
                                                    ),
                                                  ),
              'nopaging'               => false,
              'ignore_sticky_posts'    => true,
              'order'                  => 'ASC',
              'orderby'                => 'menu_order',
              'nopaging'               => true
            );

            // The Query
            $hp_hero = new WP_Query( $args );
            ?>


            <ol class="carousel-indicators">

            <?php

            if ( $hp_hero->have_posts() ) {
                
                $indicator = 0;
                while ( $hp_hero->have_posts() ) {
                  $hp_hero->the_post();
                  // do something
                  
                  
                  
                  
                 if ($indicator == 0) {

                  ?>
                     <li data-target="#hero-carousel" data-slide-to="<?php print_r($indicator); ?>" class="active indicator"></li>

                  <?php

                  
                 } else {
                  ?>
                     <li data-target="#hero-carousel" data-slide-to="<?php print_r($indicator); ?>" class="indicator"></li>

                  <?php

                 }

                 $indicator++;
                }

 


              } else {
                // no posts found
              }





            ?>

          </ol>

                    <div class="carousel-inner" role="listbox">
                      
                      <!-- #post-count:hero slider <?php print_r($hp_hero->post_count); ?> -->

                      
            <?php
             
              // The Loop
              if ( $hp_hero->have_posts() ) {
                
                $active_post = 0;
                while ( $hp_hero->have_posts() ) {
                  $hp_hero->the_post();
                  // do something
                  
                  
                  
                 if ($active_post == 0) {

                  ?>
                      <div class="item active">
                      

                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>">
                  
                     <div class="carousel-caption">
                          
                          <?php the_content(); ?> 

                          
                      </div>

                      
                  </div>

                  <?php

                  
                 } else {
                  ?>
                     <div class="item">
                        

                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>">

                    
                       <div class="carousel-caption">
                            
                            <?php the_content(); ?> 

                            
                        </div>

                        
                    </div>

                  <?php

                 }

                 $active_post++;

                }


               /*

                while ( $hp_hero->have_posts() ) {
                  $hp_hero->the_post();
                  // do something

                  ?>
               
                      <!-- second loop 

                      <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>" id="#<?php the_ID(); ?>" data-slide-to="<?php echo $active_post ?>" / >
                  
                      -->
                  <?php
                }

               */


              } else {
                // no posts found
              }

              
             

                ?>

                <!-- Controls -->
                <a class="left carousel-control" href="#hero-carousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#hero-carousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>

                 <?php

              // Restore original Post Data
              wp_reset_postdata();

           ?>

    <!-- Wrapper for slides -->
 
      </div>
       
      
    </div>

   
  </div>

</div>