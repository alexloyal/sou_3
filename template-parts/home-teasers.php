<?php
/**
 * Template part for displaying hero slider.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sou_3
 */

?>
<!-- home-teasers -->




  <div id="teasers" class="row">



    <?php 
            
            // WP_Query arguments
            $args = array (
              'post_type'              => array( 'teasers' ),
              'post_status'            => array( 'publish' ),
              'tax_query'              => array(
                                                array(
                                                      'taxonomy' => 'designs',
                                                      'field'    => 'slug',
                                                      'terms'    => 'home',
                                                    ),
                                                  ),
              'nopaging'               => false,
              'ignore_sticky_posts'    => true,
              'order'                  => 'ASC',
              'orderby'                => 'menu_order',
              'nopaging'               => true  
            );

            // The Query
            $hp_teasers = new WP_Query( $args );
            ?>


          
 
                      
            <?php
             
              // The Loop
              if ( $hp_teasers->have_posts() ) {
                while ( $hp_teasers->have_posts() ) {
                  $hp_teasers->the_post();
                  // do something
                  
                 ?>

                 <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 teaser">
                  <a href="<?php 

                              $customLink = get_post_meta( get_the_id(), 'sou-3-custom-teaser-url', true );
                              if (! empty( $customLink ) ) {
                                echo $customLink;
                              }

                    ?>" title="<?php the_title(); ?>">
                      <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>" id="<?php print strtolower(get_the_title()); ?>"  alt="<?php the_title(); ?>"/ >
                  </a>
                 </div>

                 <?php 
                  
                 

                }

 


              } else {
                // no posts found
              }

              
             

                ?>
 

                 <?php

              // Restore original Post Data
              wp_reset_postdata();

           ?>


 
      </div>
       
      
    

