<?php
/**
 * Home template file.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sou_3
 */

get_header(); ?>

			<?php 
				get_template_part( 'template-parts/hero-slider', 'hero-slider' );
				get_template_part( 'template-parts/home-teasers', 'home-teasers' );
			 ?>

<!-- home -->
	<div id="primary" class="content-area row">
		<div id="wrapper" class="col-lg-10 col-lg-offset-2 col-xs-12">
			<main id="main" class="site-main" role="main">
			
				<div id="whats_new" class="col-lg-6">
						<?php 

							// get_template_part( 'template-parts/home-content', 'home-content' );
						 ?>
				</div>

				<div id="twitter-timeline" class="col-lg-3">
					<a class="twitter-timeline" href="https://twitter.com/SouthofUrban" data-widget-id="274869217805860864">Tweets by @SouthofUrban</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

				</div>
			</main><!-- #main -->
		</div>
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
