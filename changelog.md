PROEJCT: sou_3
URL: southofurban.com
––––––––––––––––––––––

Version 0.2



Version 0.1

- First versioned chommit. Previous updates included:
	
	66ca5bc Adding custom meta box to Teasers post type.
	2 days ago
	
	3cbe434 Adding custom post type for Collections. Adding designs taxonomy to slug of individual pieces.
	2016-02-08
	
	8f5d855 Updating hero carousel height.
	2016-01-31
	
	4de5770 Adding link functionality to teasers.
	2016-01-31
	
	852c0a2 -Added branding and nav menu to footer. Also added space to add contact form or other interactivity tool to right side of foter.
	2016-01-31
	
	87e226c Updating slider post type name, adding home option under designs taxonomy, adding teasers post type. Both sliders and teasers can be deployed to other design ...
	2016-01-26
	
	6ab5af9 WordPress Query for Slider in place, slider indicators, slider content in place, need to test slider thumbnails.
	2016-01-20
	
	5f837b6 Added second hero to test slider.
	2016-01-18
	
	ee1499b Adding post ID to data-target in carousel indicator list.
	2016-01-18
	
	49d70eb Dividing homepage carousel query into three streams: Indicators, content, thumbnails.
	2016-01-18
	
	5c9a34e Adding homepage slider content type, designs taxonomy. Structuring homepage framework with boostrap grid classes.
	2016-01-18